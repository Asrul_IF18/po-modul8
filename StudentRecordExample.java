public class StudentRecordExample {
	public static void main(String[] args) {
		StudentRecord anna = new StudentRecord();
		StudentRecord elsa = new StudentRecord();
		StudentRecord olaf = new StudentRecord();

		anna.setName("Anna");
		elsa.setName("Elsa");
		olaf.setName("Olaf");

		System.out.println(anna.getName());
		System.out.println(elsa.getName());
		System.out.println(olaf.getName());

		System.out.println("Count = "+StudentRecord.getStudentCount());
	}
}