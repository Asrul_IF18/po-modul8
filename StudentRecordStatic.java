public class StudentRecordStatic {
	public String name;
	public String address;
	public int age;
	public double mathGrade;
	public double englishGrade;
	public double scienceGrade;
	public double average;
	public static int studentCount;
	
	public StudentRecordStatic() {
		studentCount++;
	}

	public StudentRecordStatic(String temp) {
		this.name = temp;
		studentCount++;
	}

	public StudentRecordStatic(String name, String Address) {
		this.name = name;
		this.address = Address;
		studentCount++;
	}

	public StudentRecordStatic(double mGrade, double eGrade, double sGrade) {
		mathGrade = mGrade;
		englishGrade = eGrade;
		scienceGrade = sGrade;
		studentCount++;
	}

	public String getName() {
		return name;
	}

	public void setName(String temp) {
		name = temp;
	}

	public String getAddress(){
		return address;
	}

	public void setAddress(String temp) {
		address = temp;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int temp) {
		age = temp;
	}
	
	public double getAverage(){
		double result = 0;
		result = (mathGrade+englishGrade+scienceGrade)/3;
		return result;
	}
	
	public static int getStudentCount() {
		return studentCount;
	}

	public void print(String temp) {
		System.out.println("Name    : "+name);
		System.out.println("Address : "+address);
		System.out.println("Age     : "+age);
	}

	public void print(double eGrade, double mGrade, double sGrade){
		System.out.println("Name : "+name);
		System.out.println("Math Grade    : "+mGrade);
		System.out.println("English Grade : "+eGrade);
		System.out.println("Science Grade : "+sGrade);
	}

	public double getEnglishGrade() {
		return englishGrade;
	}

	public void setEnglishGrade(double englishGrade) {
		this.englishGrade = englishGrade;
	}

	public double getMathGrade() {
		return mathGrade;
	}

	public void setMathGrade(double mathGrade) {
		this.mathGrade = mathGrade;
	}

	public double getScienceGrade() {
		return scienceGrade;
	}

	public void setScienceGrade(double scienceGrade) {
		this.scienceGrade = scienceGrade;
	}
}